import threading

import async_threading.async_threading as run
from flask import Flask, g, redirect, url_for, request, render_template
import flask_sijax as sijax
import os


path = os.path.join('.', os.path.dirname(__file__), 'static/js/sijax/')

app = Flask(__name__)
app.config['SIJAX_STATIC_PATH'] = path
app.config['SIJAX_JSON_URI'] = '/static/js/sijax/json2.js'
instance = sijax.Sijax(app)

launch_check = False
stop_word = [False]
check = True
lock = threading.Lock()


@sijax.route(app, "/")
def index():

    def launch_music_parser(obj_response, n):

        while stop_word.__len__() > 1:
            stop_word.pop()
        global launch_check
        global check

        if not launch_check:
            with lock:
                launch_check = True
            if check:
                check = False
                check = run.main(int(n), stop_word)
        else:
            print('Процесс сбора уже запущен!')

    def stop_music_parser(obj_response):
        global stop_word
        print(str(stop_word.__len__()) + " BEFORE")
        stop_word.append(True)
        print(str(stop_word.__len__()) + " AFTER FIRST")
        global launch_check
        launch_check = False

    # Установка страницы для перенаправления
    instance.set_request_uri(url_for('index'))

    # Декларирование ассинхронных функций (для корректного выолнения)
    instance.register_callback('launch_music_parser', launch_music_parser)
    instance.register_callback('stop_music_parser', stop_music_parser)

    # Вызов ассинхронного вывода при наличии запроса
    if instance.is_sijax_request:
        return instance.process_request()

    return render_template("index.html")

"""@app.route('/get_info', methods=['POST'])
def get_info():
    await conn.execute("SELECT count(id) FROM logs")
    logs_counter = conn.fetchone()[0]
    print(str(logs_counter) + " - кол-во записей в логах")
    return json.dumps({'status': 'OK', 'logs':logs_counter})
"""

if __name__ == '__main__':
    app.run(debug=True)
