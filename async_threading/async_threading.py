from selenium import webdriver
import asyncio
import threading
import random
import asyncpg
import time

SCROLL_PAUSE_TIME = 1
user_id = 0
uids_counter = 0
last_loaded = 0
uids = []
stop_word = []

def auth(browser):  # функция для входа на сайт OK.ru
    #lock = threading.RLock()
    #with lock:

        browser.get("https://ok.ru/")
        browser.find_element_by_id('field_email').send_keys('89505375598')  # логин (номер/email)
        browser.find_element_by_id('field_password').send_keys('89044662475')  # пароль
        browser.find_element_by_class_name('js-login-form').find_element_by_class_name('button-pro').click()

        return browser


def thr(i):  # функция описания потока для его корректного запуска и работы
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)

    loop.run_until_complete(do_stuff(stop_word))
    loop.close()


async def get_uids(stop_word):  # функция получающая уникальные ID пользователей из базы данных и распределяющая их по потокам
    await asyncio.sleep(random.uniform(0.1, 0.5))
    conn = await asyncpg.connect(user='postgres', password='123456',
                                 database='ok_users', host='192.168.18.35')
    lock = threading.RLock()

    global last_loaded

    with lock:
        limit = 1
        global user_id

        with open("last_id.txt", 'r') as last_loaded:
            for line in last_loaded:
                if line != "":

                    user_id = int(line)
                    pass

        try:
            uid = await conn.fetch('SELECT uid FROM uids WHERE id > $1 LIMIT $2', user_id, limit)
        except:
            print('Ошибка считвания ID из базы данных')
            raise

        user_id += limit

        normal_list = []
        for h in uid:
            normal_list.append(h[0])

        return normal_list


async def do_stuff(stop_word):  # функция отвечающая за полключение вебдрайвера,
                    # подключение к базе данных и создание таблицы music
    driver = webdriver.Chrome()
    driver = auth(driver)
    id_list = await get_uids(stop_word)

    await asyncio.sleep(random.uniform(0.1, 0.5))
    conn = await asyncpg.connect(user='postgres', password='123456',
                                 database='ok_users', host='192.168.18.35')
    try:
        await conn.execute("""CREATE TABLE IF NOT EXISTS music (
                track varchar(1000),
                duration varchar(10),
                user_id integer
              );""")
        await conn.execute("""CREATE TABLE IF NOT EXISTS getting_into (
                user_id integer,
                track varchar (1000)
              );""")
    except:
        print('Не удалось создать таблицу')
        raise
    await get_music(driver, id_list, conn)


async def get_music(driver, id_list, conn):
    if stop_word.__len__() > 1:
        driver.close()
        driver.quit()
        return True
    # функция отвечающая за сбор аудиозаписей пользователей сайта OK.ru
    for id in id_list:

        driver.get('https://ok.ru/profile/' + str(id) + '/music')
        time.sleep(1)
        # блок кода для прокрутки страницы браузером
        last_height = driver.execute_script("return document.body.scrollHeight")
        while True:
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            time.sleep(SCROLL_PAUSE_TIME)
            new_height = driver.execute_script("return document.body.scrollHeight")
            if (new_height == last_height):
                break
            last_height = new_height

        track_list = driver.find_elements_by_class_name('mus-tr_cnt')
        duration_list = driver.find_elements_by_css_selector('span.mus-tr_time')
        united_list = map(list, zip(track_list, duration_list))
        user_id = await conn.fetchval("SELECT id FROM uids WHERE uid=$1 ", id)
        with open("last_id.txt", 'w') as n:
            n.write(str(user_id))
        print(user_id)

        for unite in united_list:
            await conn.execute("INSERT INTO music (track,duration,user_id) VALUES (lower($1),lower($2),$3)", unite[0].text,
                               unite[1].text, user_id)
        await  conn.execute("INSERT INTO getting_into (user_id, track) SELECT user_id, music.track FROM music, extremism WHERE music.track = extremism.track")
    await get_music(driver, await get_uids(stop_word), conn)


def main(n, stop_THEWWAURLD):  # запуск потоков
    global num_threads
    global stop_word
    stop_word  = stop_THEWWAURLD
    num_threads = n
    threads = [threading.Thread(target=thr, args=(i,)) for i in range(num_threads)]
    [t.start() for t in threads]
    [t.join() for t in threads]
    print("bye")
    return True


if __name__ == "__main__":
    main()



